#!/bin/bash -e

MAINDIR=$(pwd)

cd debian/sylpheed-doc/usr/share/doc/sylpheed-doc || exit 1

mkdir FAQ
cd FAQ

for i in $MAINDIR/upstream/sylpheeddoc_faq*tar.gz; do
    lang=$(basename ${i} | cut -d '_' -f 3- | sed 's,_[0-9].*.tar.gz,,g')
    echo "Unpacking FAQ for: ${lang}"
    mkdir $lang
    cd $lang
    tar zxpf $i
    cd ..
done

cd ..

cd manual || exit 1

for i in $MAINDIR/upstream/sylpheeddoc_manual*tar.gz; do
    lang=$(basename ${i} | cut -d '_' -f 3- | sed 's,_[0-9].*.tar.gz,,g')
    echo "Unpacking manual for: ${lang}"
    mkdir $lang
    cd $lang
    tar zxpf $i
    cd ..
done

for i in $MAINDIR/upstream/sylpheed_manual*tar.gz; do
    lang=$(basename ${i} | cut -d '_' -f 3- | sed 's,_[0-9].*.tar.gz,,g')
    echo "Unpacking manual for: ${lang}"
    mkdir $lang
    cd $lang
    tar zxpf $i
    cd ..
done
